(function ($) {
    Drupal.behaviors.addressfield_de_admin = {
        attach:function (context, settings) {
            var counter = Drupal.settings.addressfield_de.counter;

            for (var i = 1; i <= counter; i++) {
                var county_id = '#address-de-county-' + i;
                var postal_code = '#address-de-postal-code-' + i;
                //var postal_code_options = 'address-de-postal-code-options' + counter[i];
                $(county_id + ' select').change(function () {
                    // Tmp disabled the hidden postal code field.
                    // @todo: remove it, if not necessary
                    //$(postal_code).hide();
                });
            }
        }
    }
})(jQuery);