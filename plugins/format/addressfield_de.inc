<?php

/**
 * @file
 * A specific handler for Hungary.
 */

$plugin = array(
  'title' => t('Address form (Germany add-on)'),
  'format callback' => 'addressfield_format_address_de_generate',
  'type' => 'address',
  'weight' => 100,
);

function addressfield_format_address_de_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'DE') {

    // Add a special html id need by ajax processes. The standard
    // drupal_html_id() can't works here, because the generated html id's are
    // passed through ajax request, there it is changed on every ajax call.
    $id_count = &drupal_static('address_de_locality_id_counter', 1);
    $county_id = "address-de-county-$id_count";
    $locality_html_id = "address-de-locality-$id_count";
    $postal_code_html_id = "address-de-postal-code-$id_count";
    variable_set('postal_code_html_id', $postal_code_html_id);
    $id_count++;

    // In original module the administrative area doesn't exisit. Add this
    // field here.
    $counties = addressfield_de_get_counties();
    $format['administrative_area'] = array(
      '#title' => t('County'),
      '#options' => $counties,
      '#attributes' => array('class' => array('county')),
    );

    // Customize the address rendering.
    if ($context['mode'] == 'render') {
      $format['country']['#weight'] = 4;
      $format['administrative_area']['#weight'] = 3;
      $format['locality_block']['#weight'] = 2;
      $format['street_block']['#weight'] = 1;


      if (isset($format['#handlers']['address-hide-country'])) {
        // Disable country, if set to hide country.
        $format['country']['#access'] = FALSE;
      }
      if (isset($format['#handlers']['address-hide-county'])) {
        // Disable county, if set to hide county
        $format['administrative_area']['#access'] = FALSE;
      }
    }
    if ($context['mode'] == 'form') {
      // Build the options for counties.
      $counties = array_merge(array('' => t('--')), $counties);
      // Extends the administrative area array with form settings.
      $format['administrative_area'] += array(
        // Ajax process to change locality autocomplete path based on this
        // field's value.
        '#ajax' => array(
          'callback' => 'addressfield_de_rebuild_locality',
          'wrapper' => $locality_html_id,
          'method' => 'replace',
        ),
        '#prefix' => '<div id="' . $county_id . '">',
        '#suffix' => '</div>',
        '#weight' => -9,
        '#size' => 10,
        '#required' => TRUE,
      );

      $format['locality_block']['#weight'] = -1;

      // Overrides of postal code fields.
      $element_postal_code = array(
        '#wrapper_id' => $format['#wrapper_id'],
        '#prefix' => '<div id="' . $postal_code_html_id . '">',
        '#suffix' => '</div>',
        '#weight' => 2,
      );

      // Overrides of locality field.
      $autocomplete_path = 'addressfield_de/autocomplete/';
      $autocomplete_path .= empty($address['administrative_area']) ? 'null' : $address['administrative_area'];
      $element_locality = array(
        '#autocomplete_path' => $autocomplete_path,
        // Add extra class for own js.
        '#attributes' => array('class' => array('addressfield-de-locality')),
        '#prefix' => '<div id="' . $locality_html_id . '">',
        '#suffix' => '</div>',
        '#size' => '20',
        '#ajax' => array(
          'callback' => 'addressfield_de_rebuild_postal_code',
          'wrapper' => $postal_code_html_id,
          'method' => 'replace',
        ),
      );


      // Merge overrides with original elements.
      $format['locality_block']['locality'] = array_merge($format['locality_block']['locality'], $element_locality);
      $format['locality_block']['postal_code'] = array_merge($format['locality_block']['postal_code'], $element_postal_code);

      // Pass some variable to own js, and add own js file. Use a named instead
      // of  __FUNCTION__, if somewhere alteration need.
      $js_settings = &drupal_static('addressfield_de_js_settings', array());
      global $base_url;
      $js_settings = array(
        'addressfield_de_fields' => array(
          $context['field']['field_name'] . '[' . $context['langcode'] . ']' . '[' . $context['delta'] . ']',
        ),
        'autocomplete_url' => $base_url . '/addressfield_de/autocomplete/null',
        'counter' => $id_count - 1,
      );
      $format['#attached']['js'][] = array('data' => drupal_get_path('module', 'addressfield_de') . '/addressfield_de.js');
      $format['#attached']['js'][] = array(
        'data' => array('addressfield_de' => $js_settings),
        'type' => 'setting',
      );
      // Add extra class for own js.
      $format['#attributes'] = array('class' => array('addressfield-de-processed'));
      // Add own validate function, where we will finalize the address array
      // before saving.
      $format['#element_validate'][] = 'addressfield_form_de_postal_code_validation';
    }
  }
  else {
    if (isset($format['locality_block']['postal_code'])) {
      // Cancel the AJAX for forms we don't control.
      $format['locality_block']['postal_code']['#ajax'] = array();
    }
  }
}

/**
 * Ajax callback if the county field changes.
 *
 * @param type $form
 * @param type $form_state
 * @return array
 *   The $format['locality_block']['locality'] part of
 *   addressfield_format_address_de_generate() array.
 */
function addressfield_de_rebuild_locality($form, $form_state) {
  $return = array();
  // $form is not completely built here.
  // The $sender['#parents'][0]][$sender['#parents'][1]][$sender['#parents'][2]
  // is the $form['field_name][language][delta] field form elemenent.
  $sender = $form_state['triggering_element'];
  $field_name = $sender['#parents'][0];
  $field_lang = $sender['#parents'][1];
  $field_delta = $sender['#parents'][2];

  // The addressfield values from $form_state
  $field_element_values = $form_state['values'][$field_name][$field_lang][$field_delta];
  // The locality element within the $form array
  $locality = $form[$field_name][$field_lang][$field_delta]['locality_block']['locality'];

  // Change the autocomplete path of locality field, and add the original ajax
  // wrapper.
  $path = empty($field_element_values['administrative_area']) ? 'addressfield_de/autocomplete/null' : 'addressfield_de/autocomplete/' . $field_element_values['administrative_area'];
  $locality['#default_value'] = '';
  $locality['#attributes']['value'] = '';
  $locality['#autocomplete_path'] = $path;
  // Change the suffix. Originally we add wrapper div to render postal code via
  // ajax. To prevent duplicate postal_code fields, unset it.
  $locality['#suffix'] = '</div>';

  return $locality;

}

/**
 * Ajax callback if the county field changes.
 *
 * @param type $form
 * @param type $form_state
 * @return array
 *   The $format['locality_block']['locality'] part of
 *   addressfield_format_address_de_generate() array.
 */
function addressfield_de_rebuild_postal_code($form, $form_state) {
  global $postal_code_html_id;
  $return = array();
  // $form is not completely built here.
  // The $sender['#parents'][0]][$sender['#parents'][1]][$sender['#parents'][2]
  // is the $form['field_name][language][delta] field form elemenent.
  $sender = $form_state['triggering_element'];
  $field_name = $sender['#parents'][0];
  $field_lang = $sender['#parents'][1];
  $field_delta = $sender['#parents'][2];

  // Get all possible postal codes for the selected locality
  $options = addressfield_de_get_postal_codes_by_locality($sender['#value']);


  if (count($options) > 1) {
    $postal_code = $form[$field_name][$field_lang][$field_delta]['locality_block']['postal_code_options'];
    $postal_code['#type'] = 'select';
    $postal_code['#title'] = t('Postal Code');
    $postal_code['#prefix'] = '<div id="' . variable_get('postal_code_html_id', '') . '">';
    $postal_code['#suffix'] = '</div>';
    $postal_code['#access'] = TRUE;
    $postal_code['#options'] = $options;
  }
  else {
    $postal_code = $form[$field_name][$field_lang][$field_delta]['locality_block']['postal_code'];
    $postal_code['#type'] = 'text';
    $postal_code['#access'] = TRUE;
    $postal_code['#default_value'] = $options;
    $postal_code['#attributes']['value'] = $options;

  }

  return $postal_code;
}

/**
 * Element validation process. This apply all of custumizations to the addresse
 * array.
 */
function addressfield_form_de_postal_code_validation($element, &$form_state, &$form) {
  // Get the base #parents for this address form.
  $base_parents = array_slice($element['#parents'], 0);
  $address = drupal_array_get_nested_value($form_state['input'], $base_parents);

  // Prepopulate postal_code, if it comes from select.
  if (!empty($address['postal_code_options'])) {
    $address['postal_code'] = $address['postal_code_options'];
    unset($address['postal_code_options']);
  }
  drupal_array_set_nested_value($form_state['values'], array_merge($base_parents, array('postal_code')), $address['postal_code'], TRUE);
}
