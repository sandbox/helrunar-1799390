<?php

/**
 * @file
 * Hide then country when only one country is available.
 */

$plugin = array(
  'title' => t('Show County on output (Germany add-on)'),
  'format callback' => 'addressfield_de_format_address_hide_county',
  'type' => 'address',
  'weight' => 101,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_de_format_address_hide_county(&$format, $address, $context = array()) {
  $format['administrative_area']['#access'] = (count($format['administrative_area']['#options']) > 1);
}
