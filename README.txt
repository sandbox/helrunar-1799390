CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------

Current Maintainer: Sven Ingo Wetsches - http://drupal.org/user/380988

This module is a plugin for Addressfield. It provides a user friendly german address form.
It extends the default Address form (country-specific) widget of Addressfield. This module based on http://drupal.org/project/addressfield_hu

This module try to make the best way to add an address by users:

    1) Choose the state from a select.
    2) The city input form is autocomplete and is filtered by the chosen state - if exists.
    3) When city is chosen, the postal code value is entered automatic.
    4) If the chosen city has more possible postal code, the postal code field become a select with the possibilities.

The rendered output is like this


Some street and street number
Something in premise field
24576 Bad Bramstedt
Schleswig-Holstein


INSTALLATION
------------

Install as usual...
see http://drupal.org/documentation/install/modules-themes/modules-7

After you activate the Module goto admin/structure/addressfield_de/import and click on "Start importing data", so
all cities with postal codes and the states will be imported into the database. This maybe will take some minutes,
please stay tuned and get a cup of coffee :-)


CONFIGURATION
-------------

* Add a field to an entity, set the field type to Postal address.
* In format handler section choose the Address form (country-specific) and Address form (German add-on) both.
* If you use the Addressfield only for german address, in the Available countries box choose Germany,
  and in format handler check the Hide the country when only one is available.
