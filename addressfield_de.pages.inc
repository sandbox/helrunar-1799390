<?php
/**
 * @file
 * Page and form generation for the addressfield_de module.
 */

function addressfield_de_config() {
  return '<p>' . t('#todo write configuration options...') . '</p>';
}

function addressfield_de_admin() {

  $header = array(
    array(
      'data' => t('Postal code'),
      'field' => 'a.postal_code',
      'sort' => 'inc',
    ),
    array(
      'data' => t('Locality'),
      'field' => 'a.locality',
    ),
    array(
      'data' => t('County'),
      'field' => 'a.administrative_area',
    ),
  );

  // Admin links.
  if (user_access('edit german address') || user_access('delete german address')) {
    $header[] = array(
      'data' => t('Links'),
    );
  }

  $query = db_select('addressfield_de', 'a')->extend('PagerDefault')->extend('TableSort');
  $query->fields('a', array('postal_code', 'locality', 'administrative_area'));

  $results = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  $rows = array();
  foreach ($results as $result) {
    $links = array();
    $rows[] = array(
      'data' =>
      array(
        // Table cells
        check_plain($result->postal_code),
        check_plain($result->locality),
        check_plain(addressfield_de_get_county_name($result->administrative_area)),
        // implode(' ', $links),
      ),
    );
  }

  $build = array();

  if (user_access('create german address')) {
    $build['add_form'] = drupal_get_form('addressfield_de_edit_form');
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No addresse available.'),
  );
  $build['pager'] = array('#theme' => 'pager');

  return $build;
}

function addressfield_de_edit($postal_code) {
  $address = addressfield_de_get_address_by_postal_code($postal_code);
  $build = drupal_get_form('addressfield_de_edit_form', $address);
  dsm(get_defined_vars());
  return $build;
}

function addressfield_de_delete($postal_code) {
  $build = drupal_get_form('addressfield_de_delete_form', $postal_code);
  return $build;
}

function addressfield_de_delete_form($form, &$form_state, $postal_code) {
  $form['postal_code'] = array(
    '#type' => 'value',
    '#value' => $postal_code,
  );
  return confirm_form($form, t('Are you sure to delete this address?'), 'admin/structure/addressfield_de', t('The address with postal code %postal_code will be deleted. This action cannot be undone.', array('%postal_code' => $postal_code)), t('Delete'), t('Cancel'));
}

function addressfield_de_delete_form_submit($form, &$form_state) {
  addressfield_de_address_delete($form_state['values']['postal_code']);
  drupal_set_message(t('The address with postal code %postal_code have been deleted.', array('%postal_code' => $form_state['values']['postal_code'])));
  watchdog('addressfield_de', 'The address with postal code %postal_code have been deleted.', array('%postal_code' => $form_state['values']['postal_code']));

  $form_state['redirect'] = 'admin/structure/addressfield_de';
  return;
}

// Disable the adminform temporary
// because we will completely rewrite it
// we will only show the entries at the moment

function addressfield_de_edit_form($form, &$form_state, $address = NULL) {

}


function addressfield_de_addresse_form_validate($form, &$form_state) {

}

function addressfield_de_edit_form_submit($form, &$form_state) {
  $address = array(
    'postal_code' => $form_state['values']['postal_code'],
    'locality' => $form_state['values']['locality'],
    'administrative_area' => $form_state['values']['administrative_area'],
  );
  if (addressfield_de_address_save($address)) {
    if ($form_state['values']['is_edit'] == 'add') {
      drupal_set_message(t('New address with postal code %postal_code have been added.', array('%postal_code' => $form_state['values']['postal_code'])));
    }
    if ($form_state['values']['is_edit'] == 'edit') {
      drupal_set_message(t('The address with postal code %postal_code have been saved.', array('%postal_code' => $form_state['values']['postal_code'])));
    }
  }
  ;

  $form_state['redirect'] = 'admin/structure/addressfield_de';
  return;
}

function addressfield_de_autocomplete($county_id, $string = '') {
  $matches = array();
  if ($string) {
    $result = db_select(ADDRESSFIELD_DE_BASE_TABLE, 'a')
      ->fields('a', array('locality'))
      ->condition('a.locality', db_like($string) . '%', 'LIKE')
      ->range(0, 25);
    if ($county_id != 'null') {
      $result->condition('administrative_area', $county_id);
    }
    $result = $result->execute();

    foreach ($result as $user) {
      $matches[$user->locality] = check_plain($user->locality);
    }
  }
  drupal_json_output($matches);
}
